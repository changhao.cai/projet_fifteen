import os
import glob
import pandas as pd
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import DataLoader, Dataset
from sklearn.model_selection import train_test_split

# Chemin vers le dossier contenant les fichiers CSV filtrés
dossier_filtres = "/Users/emmanuelbenichou/Desktop/Projet 2023 - fall detection/code/CS_dataset_filtres"

# Obtenir la liste de tous les fichiers CSV filtrés dans le dossier
chemins_csv_filtres = glob.glob(os.path.join(dossier_filtres, "*.csv"))

#-------------------------------------------------------------------------------------------
# On lit et on concatène CSV filtrés en un seul DataFrame
dfs = []
for chemin_csv in chemins_csv_filtres:
    df = pd.read_csv(chemin_csv)
    dfs.append(df)

df_concat = pd.concat(dfs, axis=1)  # Concaténation des DataFrames verticalement (en lignes)
print(df_concat)

df_labels = pd.read_csv("/Users/emmanuelbenichou/Desktop/Projet 2023 - fall detection/code/CS_dataset/labels.csv")

df_concat = pd.concat([df_concat, df_labels], axis=0)  # Ajout des données de df_labels à df_concat

#-------------------------------------------------------------------------------------------

# Séparer les données en ensembles d'entraînement et de validation (rapport 80/20)
train_df, val_df = train_test_split(df_concat, test_size=0.2, random_state=42)



# Le prétraitement des données consiste en leur normalisation
#-------------------------------------------------------------------------------------------
features = ['feature1', 'feature2', 'feature3', 'feature4', 'feature5', 'feature6', 'feature7', 'feature8']

train_df[features] = (train_df[features] - train_df[features].mean()) / train_df[features].std()
val_df[features] = (val_df[features] - val_df[features].mean()) / val_df[features].std()



#-------------------------------------------------------------------------------------------
# Classe permettant de charger les données 
class CustomDataset(Dataset):
    """
    Classe personnalisée représentant un ensemble de données personnalisé.

    Cette classe hérite de la classe `Dataset` du module `torch.utils.data`.

    ...

    Attributes
    ----------
    data : numpy.ndarray
        Tableau numpy contenant les données d'entrée.

    labels : numpy.ndarray
        Tableau numpy contenant les étiquettes correspondantes aux données d'entrée.

    Methods
    -------
    __len__()
        Retourne la taille de l'ensemble de données.

    __getitem__(index)
        Récupère un exemple spécifique de l'ensemble de données en fonction de son indice.

    """

    def __init__(self, data, labels):
        """
        Initialise une instance de la classe CustomDataset.

        Parameters
        ----------
        data : numpy.ndarray
            Tableau numpy contenant les données d'entrée.

        labels : numpy.ndarray
            Tableau numpy contenant les étiquettes correspondantes aux données d'entrée.

        """
        self.data = data
        self.labels = labels

    def __len__(self):
        """
        Retourne la taille de l'ensemble de données.

        Returns
        -------
        int
            La taille de l'ensemble de données.

        """
        return len(self.data)

    def __getitem__(self, index):
        """
        Récupère un exemple spécifique de l'ensemble de données en fonction de son indice.

        Parameters
        ----------
        index : int
            Indice de l'exemple à récupérer.

        Returns
        -------
        torch.Tensor, torch.Tensor
            Les données d'entrée et les étiquettes correspondantes sous forme de tenseurs PyTorch (équivalents de tuples).

        """
        x = torch.tensor(self.data[index], dtype=torch.float32)
        y = torch.tensor(self.labels[index], dtype=torch.long)
        return x, y

# On crée ainsi les ensembles de données et les DataLoaders pour l'entraînement et la validation
train_dataset = CustomDataset(train_df[features].values, train_df["label"].values)
# Crée un objet train_dataset de la classe CustomDataset. Cela utilise les données d'entraînement train_df[features].values
# comme données d'entrée et les étiquettes correspondantes train_df["label"].values comme étiquettes. 
# La classe CustomDataset traite ces données et étiquettes pour les rendre utilisables avec PyTorch.
val_dataset = CustomDataset(val_df[features].values, val_df["label"].values)
# Idem mais pou la validation

train_loader = DataLoader(train_dataset, batch_size=64, shuffle=True)
# Crée un objet train_loader de la classe DataLoader. Cela utilise l'objet train_dataset créé précédemment comme ensemble de données d'entraînement. 
# Le DataLoader divise les données en mini-batchs de taille 64 (batch_size=64) et les mélange (shuffle=True) pour améliorer l'apprentissage.
val_loader = DataLoader(val_dataset, batch_size=64, shuffle=False)


#-------------------------------------------------------------------------------------------
# Définir le modèle LSTM

class LSTMModel(nn.Module):
    """
    Modèle LSTM pour la classification.

    Args:
        input_size (int): Taille de la dimension d'entrée.
        hidden_size (int): Taille de la dimension cachée.
        num_layers (int): Nombre de couches LSTM.
        num_classes (int): Nombre de classes de sortie.

    Attributes:
        hidden_size (int): Taille de la dimension cachée.
        num_layers (int): Nombre de couches LSTM.
        lstm (nn.LSTM): Couche LSTM qui gère les calculs récurrents.
        fc (nn.Linear): Couche linéaire de sortie pour la classification.

    """

    def __init__(self, input_size, hidden_size, num_layers, num_classes):
        super(LSTMModel, self).__init__()
        self.hidden_size = hidden_size
        self.num_layers = num_layers
        self.lstm = nn.LSTM(input_size, hidden_size, num_layers, batch_first=True)
        self.fc = nn.Linear(hidden_size, num_classes)

    def forward(self, x):
        """
        Effectue une propagation avant à travers le modèle LSTM (dans ses différentes couches cachées).

        Args:
            x (torch.Tensor): Tenseur d'entrée de forme (batch_size, sequence_length, input_size).

        Returns:
            torch.Tensor: Tenseur de sortie de forme (batch_size, num_classes).

        Note:
            - Une passe avant (forward pass) fait passer les données d'entrée à travers les couches du modèle
              pour obtenir les prédictions. Dans ce modèle LSTM, les données d'entrée séquentielles sont
              propagées à travers une couche LSTM, et la sortie de la dernière étape temporelle est utilisée
              pour la classification à l'aide d'une couche linéaire.

            - Une couche linéaire (nn.Linear) est utilisée pour réduire les caractéristiques de la dernière séquence de sortie
              de la couche LSTM à la dimension des classes de sortie.
              Plus précisément, une couche linéaire prend en entrée un vecteur de données et effectue une multiplication matricielle 
              avec une matrice de poids, suivie d'une addition du biais. Cela peut être représenté par l'équation mathématique 
              suivante: y = W*x + b, cela permet au modèle d'apprendre des combinaisons linéaires des variables
        """
        h0 = torch.zeros(self.num_layers, x.size(0), self.hidden_size).to(device)
        c0 = torch.zeros(self.num_layers, x.size(0), self.hidden_size).to(device)
        out, _ = self.lstm(x, (h0, c0))
        out = self.fc(out[:, -1, :]) 
        # sélectionne toutes les lignes, la dernière valeur dans la deuxième dimension 
        # et toutes les colonnes de la troisième dimension de out
        return out


# Paramètres du modèle LSTM
input_size = len(features)
hidden_size = 128
num_layers = 2
num_classes = 2

# On instancie le modèle LSTM avec les paramètres précédents 
model = LSTMModel(input_size, hidden_size, num_layers, num_classes)
device = torch.device("cuda" if torch.cuda.is_available() else "cpu") # On examine si un GPU est disponible pour accélérer les calculs
model.to(device)

# Définir la fonction de perte et l'optimiseur
criterion = nn.CrossEntropyLoss() # Fonction de perte
optimizer = optim.Adam(model.parameters(), lr=0.001) # Optimiseur

# Entraînement du modèle
num_epochs = 10

for epoch in range(num_epochs):
    model.train() # Le modèle est en mode entraînement ce qui active le calcul des gradients notamment

    """

    Note: Les gradients sont des vecteurs qui représentent la direction et l'intensité du changement des poids du modèle 
    par rapport à une fonction de perte donnée. L'objectif de l'entraînement du modèle est de trouver les valeurs optimales 
    des poids qui minimisent la fonction de perte, c'est-à-dire qui permettent au modèle de mieux s'ajuster aux données d'entraînement.
    La rétropropagation du gradient est une technique importante: voir https://fr.wikipedia.org/wiki/R%C3%A9tropropagation_du_gradient 

    """

    train_loss = 0.0 
    train_correct = 0
    
    # Initialise les variables pour calculer la perte d'entraînement et la précision 
    # d'entraînement de l'époque actuelle.

    for inputs, labels in train_loader: # On itère sur les données d'entraînement
        inputs = inputs.to(device)
        labels = labels.to(device)

        optimizer.zero_grad()
        outputs = model(inputs)
        loss = criterion(outputs, labels)
        loss.backward() # Calcule les gradients de la perte par rapport aux paramètres du modèle
        optimizer.step()

        _, predicted = torch.max(outputs, 1) # On classifie selon la plus haute proba
        train_correct += (predicted == labels).sum().item()
        train_loss += loss.item()

    train_accuracy = 100.0 * train_correct / len(train_dataset)
    train_loss /= len(train_loader) # Calcule la perte d'entraînement moyenne par lot

# Évaluation sur l'ensemble de validation, le modèle passe en mode évaluation

    model.eval()
    val_loss = 0.0
    val_correct = 0

    with torch.no_grad():
        for inputs, labels in val_loader:
            inputs = inputs.to(device)
            labels = labels.to(device)

            outputs = model(inputs)
            loss = criterion(outputs, labels)

            _, predicted = torch.max(outputs, 1)
            val_correct += (predicted == labels).sum().item()
            val_loss += loss.item()

    val_accuracy = 100.0 * val_correct / len(val_dataset)
    val_loss /= len(val_loader)

    print(f"Epoch {epoch+1}/{num_epochs} - "
          f"Train Loss: {train_loss:.4f} - Train Accuracy: {train_accuracy:.2f}% - "
          f"Val Loss: {val_loss:.4f} - Val Accuracy: {val_accuracy:.2f}%")

"""

# print(f"Epoch {epoch+1}/{num_epochs} - ") : Affiche le numéro de l'époque actuelle sur le nombre total d'époques.

# f"Train Loss: {train_loss:.4f} - " : Affiche la perte d'entraînement moyenne pour l'époque actuelle, avec 4 décimales après la virgule.

# f"Train Accuracy: {train_accuracy:.2f}% - " : Affiche la précision d'entraînement pour l'époque actuelle, avec 2 décimales après la virgule et le symbole de pourcentage.

# f"Val Loss: {val_loss:.4f} - " : Affiche la perte de validation moyenne pour l'époque actuelle, avec 4 décimales après la virgule.

# f"Val Accuracy: {val_accuracy:.2f}%") : Affiche la précision de validation pour l'époque actuelle, avec 2 décimales après la virgule et le symbole de pourcentage.

"""

# Sauvegarder le modèle entraîné
torch.save(model.state_dict(), "modele_lstm.pt")
