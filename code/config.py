import os
import pandas as pd
import matplotlib.pyplot as plt

BASE_DIRECTORY = os.path.dirname(os.path.realpath(__file__))
DATASET_DIRECTORY = os.path.join(BASE_DIRECTORY, "CS_dataset")
TRIPS_DIRECTORY = os.path.join(DATASET_DIRECTORY, "trips")

def get_label_dataframe():
    return pd.read_csv(os.path.join(DATASET_DIRECTORY,"labels.csv"))

def get_trip_dataframe(trip_id: str):
    return pd.read_csv(os.path.join(TRIPS_DIRECTORY, f"{trip_id}.csv"))

# print(get_trip_dataframe('cgrabe7be75spfm9rktg'))
print(get_label_dataframe())

df = []
df0, df1 = get_trip_dataframe('cgobg6vo61i2hp2mu2t0'), get_trip_dataframe('cgqi5vfo61i2hp37j06g')
df.append(df0)
df.append(df1)
df_concat = pd.concat(df, axis=1)

print(df_concat)
