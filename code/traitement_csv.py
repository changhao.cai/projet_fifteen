import pandas as pd
import glob
import os
from code_seuils import seuil

# Chemin vers CS dataset
dossier_csv = "/Users/emmanuelbenichou/Desktop/Projet 2023 - fall detection/code/CS_dataset/trips"
dossier_reduit = "/Users/emmanuelbenichou/Desktop/Projet 2023 - fall detection/code/CS_dataset_reduits"
dossier_filtres = "/Users/emmanuelbenichou/Desktop/Projet 2023 - fall detection/code/CS_dataset_filtres"

# Obtient la liste de tous les CSV de CS dataset
chemins_csv = glob.glob(dossier_csv + "/*.csv")

#-------------------------------------------------------------------------------------------
# Créer les dossiers pour les fichiers réduits et filtrés s'ils n'existent pas
if not os.path.exists(dossier_reduit):
    os.makedirs(dossier_reduit)


if not os.path.exists(dossier_filtres):
    os.makedirs(dossier_filtres)
#-------------------------------------------------------------------------------------------

features_selectionnees = ['AYGX_mean', 'AZ_mean','timestamp']

for chemin_csv in chemins_csv:

    df = pd.read_csv(chemin_csv)

    # print(df.columns)
    # print('-' * 50)


    # Sélectionner uniquement les 8 features identifiées par Joris
    df_reduit = df[features_selectionnees]

    nom_fichier = os.path.basename(chemin_csv)

    chemin_fichier_reduit = os.path.join(dossier_reduit, nom_fichier)

    # Enregistre les données réduites dans un nouveau fichier CSV
    df_reduit.to_csv(chemin_fichier_reduit, index=False)

    df_reduit = pd.read_csv(chemin_fichier_reduit)

    # Trouver l'indice du maximum en valeur absolue de AYGX_mean
    # On peut aussi insérer ici un critère de sélection plus rigoureux de la fenêtre
#-------------------------------------------------------------------------------------------

    indice_max_abs = df_reduit[df["AYGX_max"]].abs().idxmax()

    # print(indice_max_abs)
    # print('-' * 50)

#-------------------------------------------------------------------------------------------

    # Permet d'obtenir le timestamp correspondant à l'indice maximum en valeur absolue
    timestamp_max_abs = df_reduit.loc[indice_max_abs, "timestamp"]

    borne_inf = timestamp_max_abs - 10
    borne_sup = timestamp_max_abs + 10

    # Filtre les données autour de l'instant de la chute présumée par nos soins (grâce à AYGX)
    if timestamp_max_abs + 10 & timestamp_max_abs - 10 in df_reduit["timestamp"]:

        df_filtre = df_reduit[(df_reduit["timestamp"] >= borne_inf) & (df_reduit["timestamp"] <= borne_sup)]
    
    elif timestamp_max_abs + 10 > df_reduit['timestamp'].max():

        df_filtre = df_reduit[(df_reduit["timestamp"] >= timestamp_max_abs - 20) & (df_reduit["timestamp"] <= timestamp_max_abs)]
    
    elif timestamp_max_abs - 10 < df_reduit['timestamp'].max():

        df_filtre = df_reduit[(df_reduit["timestamp"] >= timestamp_max_abs) & (df_reduit["timestamp"] <= timestamp_max_abs + 20)]

   # Obtenir l'identifiant du fichier CSV à partir du nom du fichier
    identifiant = nom_fichier.split(".csv")[0]

    # Définir le chemin pour enregistrer le nouveau fichier CSV filtré
    chemin_fichier_filtre = os.path.join(dossier_filtres, f"{identifiant}.csv")

    # Enregistrer les données filtrées dans un nouveau fichier CSV
    df_filtre.to_csv(chemin_fichier_filtre, index=False)

# À l'issue de l'execution de ce code, on se retrouve avec deux nouveaux dossiers contenant des csv centrés autour de valeurs
# intéressantes et contenant moins de variables donc prêts à être utilisés
# Je ne pense pas qu'il soit sain cependant de faire fonctionner le LSTM avec les variables issues de la PCA
# simplement car on perdrait le sens physique de l'affaire et que nous perdrions donc en lisibilité sur le fonctionnement
# du réseaux de neurones, d'autant plus que l'on perdrait des enseignements précieux (pondérations des variables physiques)
